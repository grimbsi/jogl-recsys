import os
import yaml


class YamlConfig:
    '''
    This class loads and parses a yaml configuration file for jogl-recsys having the following
    structure as illustrated here:

    user_community:
        source: 'User'
        target: 'Community'
        top_k: 3
        base_relation: 'User_has_followed_Community'
        new_relation: 'UserCommunityRecommendations'
        seen_relation: 'User_has_followed_Community'
        meta_paths:
            - weight: 0.7
              path:
                - 'User_has_skill_Skill'
                - 'inverse_Community_has_skill_Skill'
            - weight: 0.3
              path:
                - 'User_has_interest_Interest'
                - 'inverse_Community_has_interest_Interest'
    '''

    def __init__(self, config_file_path):
        '''
        Initialize a config object instance by parsing a yaml config file.
        '''
        with open(config_file_path, 'r') as config_file:
            #
            # TODO: Use yaml schema to validate the yaml file too.
            # See https://asdf-standard.readthedocs.io/en/latest/schemas/yaml_schema.html
            # The Yamale library looks cool https://github.com/23andMe/Yamale
            #
            self.config = yaml.load(config_file, Loader=yaml.SafeLoader)

    def __len__(self):
        '''
        Return the number of recommendation specifications in the config file
        '''
        return len(self.config)

    def get_recos(self):
        '''
        Return an iterator over the recommendations specifications in the config file
        '''
        return self.config.items()
