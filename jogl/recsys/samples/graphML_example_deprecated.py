import hinpy
import numpy as np
import pandas as pd
import pygraphml
import networkx

# importing HINPy (from downloaded code, or installed from pypi)
import sys
sys.path.append('path_to_hinpy/HINPy/')

######################
# CREATING HIN TABLE #
######################

# if recommendations are to be computed dynamically on the fly
# this part should be done without passing through graphml

parser = pygraphml.GraphMLParser()
g = parser.parse('JOGL_network.graphml')

hin_columns = ['relation',
               'start_group', 'start_object',
               'end_group', 'end_object',
               'value', 'timestamp']

table = pd.DataFrame(columns=hin_columns)

for ix, edge in enumerate(g.edges()):
    node1 = edge.node1.id
    node2 = edge.node2.id
    type_node1 = edge.node1.attr['d0'].value
    type_node2 = edge.node2.attr['d0'].value
    relation = edge.attr['d3'].value
    table.loc[ix, :] = [relation, type_node1, node1, type_node2, node2, '', '']


# Up until here, data model does not match the format needed for
# HIN recommender systems, because there are relations that have edges
# that don't have the same starting or ending type of node.
# For example: "has_mentioned" connects Posts to Projects, but sometimes
# Posts to Users. There needs to be a specialized relation for each.


# Here I append the names of the types of connected entities to the relation
# in order to specialized them, creating some cumbersome names.
table.loc[:, 'relation'] = table.apply(
    lambda row: row.relation+'_'+row.start_group+'_'+row.end_group, axis=1)

###########################
# CREATING THE HIN OBJECT #
###########################


hin = hinpy.classes.HIN(table=table, inverse_relations=True, verbose=False)

# the flag "inverse_relations" creates for each relation, the inverse one.
# If "has_mentioned" connects Posts to Users, "inverse_has_mentioned"
# (following hinpy naming convention) connects Users to Posts in what would
# be semantically equivalent to "is mentioned in".


#################################
# PRODUCING HIN RECOMMENDATIONS #
#################################

# For this example, I compute recommendations proposing
# communities to users, based on:
# 1: users' interests that are also shared by communities
# 2: users' skills that are also present in communities
# ... so a mixture of two meta-paths (both start with User and end with Community!)

meta_path_1 = ['has_interest_User_Interest',
               'inverse_has_interest_Community_Interest']
meta_path_2 = ['has_skill_User_Skill', 'inverse_has_skill_Community_Skill']

# ... to be mixed in 70%-30% rates

# We define a dictionary with the parameters
params = {'method': 'CB',  # <- is the meta-path-based RS
          # <- number of items to propose in recommendation for each entity (typically users)
          'topK_predictions': 3,
          # <- a relation that stores things users has seen, or chosen, so as to not recommend again
          'seen_relation': 'has_followed_User_Community',
          # This is the semantic part: list of meta-paths (list of link groups) to include
          'paths': [meta_path_1, meta_path_2],
          'paths_weights': [0.7, 0.3]}

# Computing recommendations is done by creating a new relation that
# each recommendation as an edge (connecting a User with a Community)
hin.CreateLinkGroupFromRS(relation_name='has_followed_User_Community',  # this is the base relation from which to compute recos using meta-paths
                          # the name of the new relation containing recos
                          new_relation_name='UserCommunityRecommendations',
                          parameters=params)

# to retrieve a table with the recommendations:

recos1 = hin.table[hin.table.relation ==
                   'UserCommunityRecommendations'].copy(deep=True)

# for different example, let us include a more complex meta-path for
# illustrative purposes (it might not have that much semantic meaning?)

meta_path_3 = ['is_author_User_Post',
               'has_mentioned_Post_User', 'has_clapped_User_Community']

# ... the semantic content would be: recommend the communities that have been clapped
#     by users mentioned by the user to which we are computing recommendations


params = {'method': 'CB',
          'topK_predictions': 3,
          'seen_relation': 'has_followed_User_Community',
          'paths': [meta_path_1, meta_path_2, meta_path_3],
          'paths_weights': [0.7, 0.2, 0.1]}


hin.CreateLinkGroupFromRS(relation_name='has_followed_User_Community',  # this is the base relation from which to compute recos using meta-paths
                          # the name of the new relation containing recos
                          new_relation_name='UserCommunityRecommendations2',
                          parameters=params)

# to retrieve a table with the recommendations:

recos2 = hin.table[hin.table.relation ==
                   'UserCommunityRecommendations2'].copy(deep=True)


###############################
# GETTING ALL THE SCORES      #
###############################

# If you have a meta-path for some semantic, you can retrieve all the scores:

scores1 = hin.proportional_abundances(path=meta_path_1)

#  So now scores is a M x N sparse matrix, with
#  M=number of start objects (2226 users in this case)
#  N=number of end objects (20 communities in this case)
#  This matrix could well be computed every x minutes,
#  so, community scores for user 0 are

np.asarray(scores1[0, :].todense()).reshape(-1)

# The thing is that will need to deal with sparse matrix formats, else
# you will be storing huge matrices. This might be something to think
# when deciding whether to store the first topK results, or the whole
# spare matrix.

# By the way, to get to row and column position of users and communities:

User_dictionary = hin.GetObjectGroup('User').OjectPositionDicFromName()
Community_dictionary = hin.GetObjectGroup(
    'Community').OjectPositionDicFromName()

# And if you want to mix meta-paths, you can just poderate them:

scores2 = hin.proportional_abundances(path=meta_path_2)

mixed_scores = 0.7*scores1+0.3*scores2

# and retrieve the scores as before

np.asarray(mixed_scores[0, :].todense()).reshape(-1)

# ... not as pretty as before. Maybe I could wrap these functionalities in a
# method if it fits better into the pipeline strategy.
