to_hinpy = {
    'sourceable_node_type': 'start_group',
    'sourceable_node_id': 'start_object',
    'targetable_node_type': 'end_group',
    'targetable_node_id': 'end_object',
    'relation_type': 'relation',
    'updated_at': 'timestamp'
}

to_jogl = {v: k for (k, v) in to_hinpy.items()}

hinpy_names = to_hinpy.keys()

jogl_names = to_hinpy.values()

hin_columns = [
    'relation',
    'start_group',
    'start_object',
    'end_group',
    'end_object',
    'value',
    'timestamp'
]

from .store import Store as Store
from .csv import CsvFile as CSVStore
from .database import JoglDatabase as DatabaseStore
from .network import JoglNetworkApi as NetworkApiStore
