import click
import pandas as pd
import requests
import sys

from ttictoc import Timer

from jogl.recsys.stores import hin_columns, Store


class JoglNetworkApi(Store):
    def __init__(self, endpoint='https://jogl-backend.herokuapp.com/api/networks', result_path="./network_prediction.csv", network_id=None):
        # TODO: Add a method to cache the network locally to avoid having to do long API calls at each iteration
        super().__init__()
        self.network_id = network_id
        self.endpoint = endpoint
        self.result_path = result_path

    def preprocess_row(self, row):
        [type_node1, node1] = row.source.split('_')
        [type_node2, node2] = row.target.split('_')
        # Here I append the names of the types of connected entities to the relation
        # in order to specialized them, creating some cumbersome names.
        relation = type_node1+'_'+row.relation_type+'_'+type_node2
        timestamp = row.updated_at
        timestamp = timestamp if timestamp else ''
        return [relation, type_node1, node1, type_node2, node2, row.value, timestamp]

    def get_network_ids(self):
        """
          Returns an array of available networks.  Each network is a dict holding the following properties:
          - id
          - edges
          - created_at
          - nodes
          - updated_at
        """
        response = requests.get(self.endpoint)
        if response.status_code != 200:
            sys.exit('Cannot get list of networks at {}. Status code: {}'.format(
                self.endpoint, response.status_code))
        json = response.json()
        return [network['id'] for network in json]

    def get_edges(self, network_id):
        """
          Returns an dataframe of edges having the following columns:
          - source
          - source_type
          - target
          - target_type
          - relation_type
          - value
          - created_at
          - updated_at
        """

        with Timer(verbose=False) as T:
            click.echo(
                'Will get edges for network {}'.format(network_id))
            response = requests.get('{}/{}'.format(self.endpoint, network_id))
            if response.status_code != 200:
                sys.exit('Cannot get edges for network {} at {}. Status code: {}'.format(
                    network_id, self.endpoint, response.status_code))
            json = response.json()

            edges = pd.read_csv(json['edges'])

        click.echo('{} edges downloaded in {:.1f}s ({:.1f} edges/s)'.format(len(edges),
                                                                            T.elapsed, len(edges) / T.elapsed))

        return edges

    def preprocess_edges(self, edges):
        # The JOGL edge data model does not match the format needed for
        # HIN recommender systems, because there are relations that have edges
        # that don't have the same starting or ending type of node.
        # For example: "has_mentioned" connects Posts to Projects, but sometimes
        # Posts to Users. There needs to be a specialized relation for each.
        with Timer(verbose=False) as T:
            click.echo('Will preprocess {} edges'.format(len(edges)))
            df = edges.apply(self.preprocess_row, axis=1, result_type='expand')
            df.columns = hin_columns

        click.echo('{} edges processed in {:.1f}s ({:.1f} edges/s)'.format(len(edges),
                                                                           T.elapsed, len(edges) / T.elapsed))
        return df

    def load_data(self):
        '''
        Construct hin table using network data from JOGL
        '''
        # Local state caching to avoid redoing API calls
        if self.data is not None:
            return self.data
        network_ids = self.get_network_ids()
        click.echo('Available networks on API: {}'.format(len(network_ids)))

        if not self.network_id:
            self.network_id = network_ids[-1]
        edges = self.get_edges(self.network_id)
        df = self.preprocess_edges(edges)
        click.echo('Total number of edges: {}'.format(len(df)))
        self.data = df
        return df

    def save_results(self):
        self.results.to_csv(self.result_path, index=False)
