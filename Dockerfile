# Base docker image
FROM python:latest

# Use app dir
WORKDIR /usr/src/app

# Install dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Add source files
COPY ./jogl ./jogl

# Run example for now
CMD [ "python", "-m", "jogl.recsys" ]
